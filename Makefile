.PHONY: prepare start install ps down logs pull-frontend start-frontend \
	pull-api start-api api-deploy

prepare:
	@touch acme.json &&\
	chmod 600 acme.json &&\
	cp .env.dist .env

start:
	@docker-compose up -d reverse-proxy

install: prepare start

ps:
	@docker-compose ps

down:
	@docker-compose down

logs:
	@docker-compose logs

pull-frontend:
	@docker-compose pull frontend

start-frontend:
	@docker-compose up -d frontend

pull-api:
	@docker-compose pull api

start-api:
	@docker-compose up -d api

api-deploy: pull-api start-api
